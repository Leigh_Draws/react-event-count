import './App.css'
import './components/Counter.css'
import Counter from './components/Counter'


function App () {
  return (
    <>
    <section className='container-general'>
      <Counter 
      title = "Bal Masqué"/>
      <Counter 
      title = "Patinoire à ciel ouvert"/>
      <Counter 
      title = "Concours de patates"/>
    </section>
    </>
  )
}

export default App
