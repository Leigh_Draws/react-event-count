import { useState } from 'react'

function Counter(props) {

// variable const => constante qui ne peut pas être réassignée plus après //
//    [valeur contenue, fonction pour changer la valeur] = fonction valeur initiale (valeur initiale)
    const [count, setCount] = useState(0)
  
    return (
      <>
      <section className='bloc'>
        <div className='count'>
          <h1>{props.title}</h1>
          <h2>{count} participants</h2>
        </div>
          <div className='container'>
            <div className='card'>
              <button onClick={() => { setCount (count + 1) }}>+</button>
            </div>
            <div className='card'>
              <button onClick={() => { count > 0 ? setCount (count -1) : count}}>-</button>
            </div>
            <div className='card'>

{/* Reset du compteur : La valeur de count - la valeur de count // Si j'avais mis un let plus haut j'aurai pu faire count = 0*/}
              <button onClick={() => { setCount (count - count) }}>Reset</button>
            </div>
          </div>
      </section>
      </>
    )
  }
  
  export default Counter;